class Api::ActionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_action
  
  def show
    render json: @action.to_json(include:[:qualifiers])
  end
  
  private
  def set_action
      @action = Action.find(params[:id])
  end
  
  
  def action_params
    params.require(:action).permit(:id, :name, qualifiers_attributes: [:id, :action_id, :name, :datatype])
  end
end
