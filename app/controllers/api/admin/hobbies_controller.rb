class Api::Admin::HobbiesController < ApplicationController
	before_action :authenticate_user!
	  before_action :set_hobby, only: [:show, :update, :destroy]

	  # GET /hobbies
	  # GET /hobbies.json
	  def index
	    @hobbies = Hobby.all

	    render json: @hobbies
	  end

	  # GET /hobbies/1
	  # GET /hobbies/1.json
	  def show
	    render json: @hobby
	  end

	  # POST /hobbies
	  # POST /hobbies.json
	  def create
	    @hobby = Hobby.new(hobby_params)

	    if @hobby.save
	      render json: @hobby, status: :created, location: @hobby
	    else
	      render json: @hobby.errors, status: :unprocessable_entity
	    end
	  end

	  # PATCH/PUT /hobbies/1
	  # PATCH/PUT /hobbies/1.json
	  def update
	    @hobby = Hobby.find(params[:id])

	    if @hobby.update(hobby_params)
	      head :no_content
	    else
	      render json: @hobby.errors, status: :unprocessable_entity
	    end
	  end

	  # DELETE /hobbies/1
	  # DELETE /hobbies/1.json
	  def destroy
	    @hobby.destroy

	    head :no_content
	  end

	  private

	    def set_hobby
	      @hobby = Hobby.find(params[:id])
	    end

	    def hobby_params
	      params[:hobby]
	    end
end
