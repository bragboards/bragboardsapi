class Api::BetaRequestsController < ApplicationController
	before_action :authenticate_user!, only: [:index, :show, :update, :destroy]
	before_action :set_beta_request, only: [:show, :update, :destroy]

  # GET /beta_requests
  # GET /beta_requests.json
  def index
    @beta_requests = BetaRequest.all

    render json: @beta_requests
  end

  # GET /beta_requests/1
  # GET /beta_requests/1.json
  def show
    render json: @beta_request
  end

  # POST /beta_requests
  # POST /beta_requests.json
  def create
    @beta_request = BetaRequest.new(beta_request_params)

    if @beta_request.save
      render json: @beta_request, status: :created
    else
      render json: @beta_request.errors, status: :unprocessable_entity
    end
  end


  def check_email
    render json: BetaRequest.find_by_email(params[:email]).nil?.to_json
  end

  # PATCH/PUT /beta_requests/1
  # PATCH/PUT /beta_requests/1.json
  def update
    @beta_request = BetaRequest.find(params[:id])

    if @beta_request.update(beta_request_params)
      head :no_content
    else
      render json: @beta_request.errors, status: :unprocessable_entity
    end
  end

  # DELETE /beta_requests/1
  # DELETE /beta_requests/1.json
  def destroy
    @beta_request.destroy

    head :no_content
  end

  private

    def set_beta_request
      @beta_request = BetaRequest.find(params[:id])
    end

    def beta_request_params
      params.permit(:name, :email, :dob, :location)
    end
end
