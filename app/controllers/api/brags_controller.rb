class Api::BragsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_brag, only: [:show, :update, :destroy]

  # GET /brags
  # GET /brags.json
  def index
    @brags = Brag.all

    render json: @brags
  end

  # GET /brags/1
  # GET /brags/1.json
  def show
    render json: @brag
  end

  # POST /brags
  # POST /brags.json
  def create
    @brag = Brag.new(brag_params)

    if @brag.save
      render json: @brag, status: :created, location: @brag
    else
      render json: @brag.errors
    end
  end

  # PATCH/PUT /brags/1
  # PATCH/PUT /brags/1.json
  def update
    @brag = Brag.find(params[:id])

    if @brag.update(brag_params)
      head :no_content
    else
      render json: @brag.errors, status: :unprocessable_entity
    end
  end

  # DELETE /brags/1
  # DELETE /brags/1.json
  def destroy
    @brag.destroy

    head :no_content
  end

  private

    def set_brag
      @brag = Brag.find(params[:id])
    end

    def brag_params
      params[:brag]
    end
end
