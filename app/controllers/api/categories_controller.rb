class Api::CategoriesController < ApplicationController
	before_action :authenticate_user!
  before_action :set_category, only: [:show]

  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all

    render json: @categories.as_json(include:[:hobbies])
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    render json: @category.as_json(include:[:hobbies])
  end

  private

    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params[:category]
    end
end
