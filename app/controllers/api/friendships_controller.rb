class Api::FriendshipsController < ApplicationController
  before_action :authenticate_user!
	before_action :set_friendship, only: [:show, :destroy]

  def index
    if params[:user_id] and params[:friend_id]
      @friendships = Friendship.find_by_user_id_and_friend_id(params[:user_id],params[:friend_id])
    else
      @friendships = current_user.friendships.all
    end
    render json: @friendships.to_json(include:[:user, :friend, :inverse])
  end

  def create
  	@friendship = current_user.friendships.build(friend_id: params[:friendship][:friend_id])
  	if @friendship.save
  		render json: @friendship.to_json(include:[:user, :friend, :inverse]), status: :created
    else
      render json: @friendship.errors, status: :unprocessable_entity
    end

  end

  def show
    render json: @friendship.to_json(include:[:user, :friend, :inverse]), status: :created
  end

  def update
  	@friendship = current_user.friendships.build(friend_id: params[:friendship][:user_id], inverse_id: params[:friendship][:id])
  	@request = Friendship.find(params[:friendship][:id])
  	if @friendship.save
  		@request.inverse_id = @friendship.id
  		if @request.save
  			render json: @friendship.to_json(include:[:user, :friend, :inverse]), status: :created
  		else
  			render json: @request.errors, status: :unprocessable_entity
  		end
  	else
  		render json: @friendship.errors, status: :unprocessable_entity
  	end
  end

  def destroy
    if !@friendship.inverse.nil?
      @friendship.inverse.destroy
    end
    @friendship.destroy
    render json: @friendship.to_json(include:[:user, :friend, :inverse])
  end

  private

  def set_friendship
    @friendship = Friendship.find_by_id(params[:id])
  end

  def user_params
    params.require(:friendship).permit(:user_id, :friend_id, :inverse_id)
  end
end
