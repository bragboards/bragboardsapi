class Api::HobbiesController < ApplicationController
	before_action :authenticate_user!
	  before_action :set_hobby, only: [:show]

	  # GET /hobbies
	  # GET /hobbies.json
	  def index
	    @hobbies = Hobby.all

	    render json: @hobbies
	  end

	  # GET /hobbies/1
	  # GET /hobbies/1.json
	  def show
	    render json: @hobby
	  end

	  private

	    def set_hobby
	      @hobby = Hobby.find(params[:id])
	    end

	    def hobby_params
	      params[:hobby]
	    end
end
