class Api::LocationsController < ApplicationController
	
	before_action :authenticate_user!
	before_action :set_user, only: [:index, :create]
  before_action :set_location, only: [:show, :update, :destroy]

	def index
    @locations = @user.locations.all

    render json: @locations.to_json(include:[:hobbies, :region])
  end

  def create
  	@location = @user.locations.build(location_params)

  	if @location.save
  		render json: @location.to_json(include:[:hobbies, :region]), status: :created
  	else
    		render json: @location.errors, status: :unprocessable_entity
  	end
  end

  # GET /locations/1
  # GET /locations/1.json
  def show
    render json: @location.to_json(include:[:hobbies, :region])
  end

  def update
    if @location.update(location_params)
      render json: @location.to_json(include:[:hobbies, :region])
    else
      render json: @brag.errors, status: :unprocessable_entity
    end
  end

  def destroy
  	@location.destroy
  	render json: @location.to_json(include:[:hobbies, :region])
  end

  private
  	def set_user
  		if params[:user_id]
  			@user = User.find(params[:user_id])
  		else
  			@user = User.find(params[:location][:user_id])
  		end
  	end

    def set_location
      @location = Location.find(params[:id])
    end

    def location_params
      params.require(:location).permit(:id, :user_id, :location_id, :name, :geolocation, :latitude, :longitude, :description, :image, :radius, :private)
    end
end
