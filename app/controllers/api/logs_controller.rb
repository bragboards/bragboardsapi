class Api::LogsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:index, :create]
  before_action :set_log, only: [:show, :update, :destroy]
  
  def index
    @logs = Log.where(user_id: @user.id)
    render json: @logs.to_json(include:[:user, :location, :hobby, :category, :record_sets])
  end
  
  def create
    @log = Log.create(log_params)
    if @log.save
      @record_set = @log.record_sets.build(user_id: @log.user_id)
      @record_set.save
      render json: @log.to_json(include:[:user, :location, {:hobby => {:include => {:actions => {:include => :qualifiers}}}}, :category, {:record_sets => {:include => {:records => {:include => :value_datum}}}}])
    else
      render json: @log.errors, status: :unprocessable_entity
    end
  end
  
  def show
    render json: @log.to_json(include:[:user, :location, {:hobby => {:include => {:actions => {:include => :qualifiers}}}}, :category, {:record_sets => {:include => {:records => {:include => :value_datum}}}}])
  end
  
  def update
    if @log.update(log_params)
      render json: @log.to_json(include:[:user, :location, :hobby, :category, :record_sets])
    else
      render json: @log.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    @log.destroy
    render json: @log.to_json(include:[:user, :location, :hobby, :record_sets])
  end
  
  private
  	def set_user
      if  params[:user_id]
        @user = User.find(params[:user_id])
      else
  		  @user = User.find(params[:log][:user_id])
      end
  	end

    def set_log
      @log = Log.find(params[:id])
    end

    def log_params
      params.require(:log).permit(:id, :user_id, :location_id, :hobby_id)
    end
end
