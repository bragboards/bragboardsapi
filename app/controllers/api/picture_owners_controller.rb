class Api::PictureOwnersController < ApplicationController
	before_action :authenticate_user!
  before_action :set_picture_owner, only: [:index]

  def index
  	render json: @picture_owner
  end
  
  def destroy
  	@picture_owner = PictureOwner.find(params[:id])
  	@picture_owner.destroy
  	render json: @picture_owner
  end

  private
  def set_picture_owner
  	@picture_owner = PictureOwner.find_by_imageable_id_and_imageable_type_and_picture_id(params[:imageable_id], params[:imageable_type], params[:picture_id])
  end

  def picture_owner_params
  	params.permit(:imageable_id, :imageable_type, :picture_id, :id)
  end
end
