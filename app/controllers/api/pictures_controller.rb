class Api::PicturesController < ApplicationController
	before_action :authenticate_user!
  before_action :set_owner, only: [:index, :create]
  before_action :set_picture, only: [:show, :destroy]

  # GET /Pictures
  # GET /Pictures.json
  def index
    @pictures = @owner.pictures.all

    render json: @pictures
  end

  def create
  	picture_params[:avatar] = decode_picture_data(picture_params[:avatar])
  	@picture = Picture.new
  	@picture.avatar = picture_params[:avatar]
		if @picture.save!
  		@owner.pictures<<@picture
  		if @picture_user
  			@picture_user.pictures<<@picture
  		end
  		render json: @picture, status: :created
  	else
    	render json: @picture.errors, status: :unprocessable_entity
  	end
  end

  # GET /Pictures/1
  # GET /Pictures/1.json
  def show
    render json: @picture
  end

  def destroy
  	@picture.destroy
  	render json: @picture
  end

  private

    def set_owner
    	if params[:user_id]
    		@owner = User.find(params[:user_id])
    	elsif picture_params[:user_id]
    		@owner = User.find(picture_params[:user_id])
    	elsif params[:location_id]
    		@owner = Location.find(params[:location_id])
    		@picture_user = @owner.user
    	elsif picture_params[:location_id]
    		@owner = Location.find(picture_params[:location_id])
    		@picture_user = @owner.user
      elsif params[:record_id]
        @owner = Record.find(params[:record_id])
        @picture_user = @owner.record_set.user
      elsif picture_params[:record_id]
        @owner = Record.find(picture_params[:record_id])
        @picture_user = @owner.record_set.user
    	end
      
    end

    def set_picture
    	@picture = Picture.find(params[:id])
    end

    def picture_params
    	if params[:picture]
        params.require(:picture).permit(:user_id, :location_id, :record_id, :avatar)
    	else
    		 params.permit(:user_id, :location_id, :avatar)
    	end
    end

    def decode_picture_data picture_data
    # decode the base64
    data = StringIO.new(Base64.decode64(picture_data))

    # assign some attributes for carrierwave processing
    data.class.class_eval { attr_accessor :original_filename, :content_type }
    data.original_filename = "upload.png"
    data.content_type = "image/png"

    # return decoded data
    data
		end
end
