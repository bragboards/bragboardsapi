class Api::QualifiersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_qualifier
  
  def show
    render json: @qualifier
  end
  
  private
  def set_qualifier
      @qualifier = Qualifier.find(params[:id])
  end
  
  
  def qualifier_params
    params.require(:qualifier).permit(:id, :name, :datatype)
  end
end
