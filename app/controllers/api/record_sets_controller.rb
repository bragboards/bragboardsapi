class Api::RecordSetsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_log, only: [:index]
  before_action :set_user, only: [:create]
  before_action :set_record_set, only: [:destroy]
  
  def index
    @record_sets = @log.record_sets
    render json: @record_sets.to_json(include:[:user,:log, :location, :hobby, :category, {:records => {:include => [:action, {:value_datum => {:include => :qualifier}}, :pictures]}}])
  end
  
  def create
    @record_set = RecordSet.create(record_set_params)
    if @record_set.save
      render json: @record_set.to_json(include:[:user,:log, :location, :hobby, :category, {:records => {:include => [:action, {:value_datum => {:include => :qualifier}}, :pictures]}}])
    else
      render json: @record_set.errors, status: :unprocessable_entity
    end
  end

  def destory
    @record_set.destroy
    render json: @record_set.to_json(include:[:user,:log, :location, :hobby, :category, {:records => {:include => [:action, {:value_datum => {:include => :qualifier}}]}}])
  end
  
  private
  def set_user
    if  params[:user_id]
      @user = User.find(params[:user_id])
    else
      @user = User.find(params[:record_set][:user_id])
    end
  end
    
  def set_log
    if  params[:log_id]
      @log = Log.find(params[:log_id])
    else
      @log = Log.find(params[:record_set][:log_id])
    end
  end
    
  def set_record_set
    @record_set = RecordSet.find(params[:id])
  end
  
  def record_set_params
    params.require(:record_set).permit(:id, :user_id, :log_id)
  end
end
