class Api::RecordsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_record_set, only: [:index, :create]
  before_action :set_record, only:[:update, :destroy]
  
  def index
    @records = @record_set.records
    render json: @records.to_json(include:[:action, :pictures])
  end
  
  def create
    @record = @record_set.records.build(record_params)
    if @record.save
      render json: @record.to_json(include:[:action, :pictures])
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end
  
  def update
    if @record.update(record_params)
      render json: @record.to_json(include:[:action, :pictures, {:value_datum => {:include => :qualifier}}])
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @record.destroy
    render json: @record.to_json(include:[:action, :pictures])
  end
  
  private
  def set_record_set
    if  params[:record_set_id]
      @record_set = RecordSet.find(params[:record_set_id])
    else
      @record_set = RecordSet.find(params[:record][:record_set_id])
    end
  end
  def set_record
    @record = Record.find(params[:id])
  end
  
  def record_params
    params.require(:record).permit(:id, :record_set_id, :action_id, :comment, :created_at, :updated_at, value_datum_attributes: [:id, :record_id, :qualifier_id, :value, :created_at, :updated_at],value_datum: [:id, :record_id, :qualifier_id, :value, :created_at, :updated_at])
  end
end
