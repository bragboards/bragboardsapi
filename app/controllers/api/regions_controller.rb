class Api::RegionsController < ApplicationController
	before_action :authenticate_user!
  before_action :set_region, only: [:show, :destroy]

  # GET /Regions
  # GET /Regions.json
  def index
    @regions = Region.all

    render json: @regions.to_json(include:[:locations])
  end

  def create
  	@region = Region.create(region_params)

  	if @region.save
  		render json: @region.to_json(include:[:locations]), status: :created
  	else
    		render json: @region.errors, status: :unprocessable_entity
  	end
  end

  # GET /Regions/1
  # GET /Regions/1.json
  def show
    render json: @region.to_json(include:[:locations])
  end

  def destroy
  	@region.destroy
  	render json: @region.to_json(include:[:locations])
  end

  private

    def set_region
      @region = Region.find(params[:id])
    end

    def region_params
      params.require(:region).permit(:id, :name, :geolocation, :latitude, :longitude, :description, :image, :radius)
    end
end
