class Api::UserHobbiesController < ApplicationController
	before_action :authenticate_user!
  before_action :set_user, only: [:index, :create]
  before_action :set_user_hobby, only: [:show, :update, :destroy]

  # GET /user_hobbies
  # GET /user_hobbies.json
  def index
    if params[:user_id] and params[:hobby_id]
      @user_hobbies = UserHobby.where(user_id: params[:user_id], hobby_id: params[:hobby_id])
    else
      @user_hobbies = @user.user_hobbies.all
    end
    render json: @user_hobbies.to_json(include:[:user, :hobby])
  end

  # GET /user_hobbies/1
  # GET /user_hobbies/1.json
  def show
    render json: @user_hobby.to_json(include:[:user, :hobby])
  end

  # POST /user_hobbies
  # POST /user_hobbies.json
  def create
    @user_hobby = @user.user_hobbies.build({hobby_id: params[:user_hobby][:hobby_id]})

    if @user_hobby.save
      render json: @user_hobby.to_json(include:[:user, :hobby]), status: :created
    else
      render json: @user_hobby.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /user_hobbies/1
  # PATCH/PUT /user_hobbies/1.json
  def update
  	
    if @user_hobby.update(user_hobby_params)
      head :no_content
    else
      render json: @user_hobby.errors, status: :unprocessable_entity
    end
  end

  # DELETE /user_hobbies/1
  # DELETE /user_hobbies/1.json
  def destroy
    @user_hobby.destroy

    render json: @user_hobby.to_json(include:[:user, :hobby])
  end

  private
  	def set_user
      if  params[:user_id]
        @user = User.find(params[:user_id])
      else
  		  @user = User.find(params[:user_hobby][:user_id])
      end
  	end

    def set_user_hobby
      @user_hobby = UserHobby.find(params[:id])
    end

    def user_hobby_params
      params.require(:user_hobby).permit(:id, :user_id, :hobby_id)
    end
end
