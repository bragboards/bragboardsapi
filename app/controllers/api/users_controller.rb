class Api::UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    if params[:email]
      @users = User.where('email LIKE ?',params[:email])
    elsif params[:id]
      @users = User.find_by_id(params[:id])
    else
      @users = User.all
    end

    render json: @users.to_json(include:[:hobbies, :friends, :friendships, :requests, :requested_friends, :locations])
  end

  # GET /users/1
  # GET /users/1.json
  def show
    render json: @user.to_json(include:[:hobbies, :friends, :friendships, :requests, :requested_friends, :locations])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    head :no_content
  end

  def search
    if params[:search_type] == 'notFriends' and current_user.friends.count != 0
      friend_ids = current_user.friends.map {|f| f.id}
      friend_ids = friend_ids -[current_user.id]
      @users = User.where('(first_name LIKE ? or last_name LIKE ? or email LIKE ? or location LIKE ?) and id NOT IN (?)',"%#{params[:term]}%","%#{params[:term]}%","%#{params[:term]}%","%#{params[:term]}%",friend_ids)
    elsif params[:search_type] =='friends' and current_user.friends.count != 0
      friend_ids = current_user.friends.map {|f| f.id}
      @users = User.where('(first_name LIKE ? or last_name LIKE ? or email LIKE ? or location LIKE ?) and id IN (?)',"%#{params[:term]}%","%#{params[:term]}%","%#{params[:term]}%","%#{params[:term]}%",friend_ids)
    else
      @users = User.where('(first_name LIKE ? or last_name LIKE ? or email LIKE ? or location LIKE ?) and id!=?',"%#{params[:term]}%","%#{params[:term]}%","%#{params[:term]}%","%#{params[:term]}%",current_user.id)
    end
    render json: @users
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :address1, :address2, :city, :state, :zipcode, :password, :user_type_id)
    end
end
