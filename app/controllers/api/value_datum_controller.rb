class Api::ValueDatumController < ApplicationController
  before_action :authenticate_user!
  before_action :set_record, only: [:index, :create]
  
  def index
    @value_datum = @record.value_datum
    render json: @value_datum.to_json(include:[:qualifier])
  end
  
  def create
    @value_data = @record.value_datum.build(value_data_params)
    if @value_data.save
      render json: @value_data.to_json(include:[:qualifier])
    else
      render json: @value_data.errors, status: :unprocessable_entity
    end
  end
  
  def update
    if @value_data.update(value_data_params)
      render json: @value_data.to_json(include:[:qualifier])
    else
      render json: @value_data.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @value_data.destroy
    render json: @value_data.to_json(include:[:qualifier])
  end
  
  private
  def set_record
    if  params[:record_id]
      @record = Record.find(params[:record_id])
    else
      @record = Record.find(params[:value_data][:record_id])
    end
  end
  def set_value_data
    @value_data = ValueData.find(params[:id])
  end
  
  def value_data_params
    params.require(:value_data).permit(:id, :record_id, :created_at, :updated_at, :qualifier_id, :value)
  end
end
