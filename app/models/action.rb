class Action < ActiveRecord::Base
	has_many :hobby_actions
	has_many :hobbies, through: :hobby_actions

  has_many :qualifiers, dependent: :destroy
	has_many :records
  
  accepts_nested_attributes_for :qualifiers
end
