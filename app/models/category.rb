class Category < ActiveRecord::Base
	has_many :hobbies
  has_many :logs, through: :hobbies
  has_many :logs, through: :hobbies
  has_many :locations, through: :hobbies
	accepts_nested_attributes_for :hobbies
end
