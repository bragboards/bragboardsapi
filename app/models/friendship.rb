class Friendship < ActiveRecord::Base
	belongs_to :user
	belongs_to :friend, class_name: "User"
	has_one :inverse, class_name: "Friendship", foreign_key: 'inverse_id'
end
