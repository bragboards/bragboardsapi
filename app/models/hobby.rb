class Hobby < ActiveRecord::Base
	belongs_to :category
	has_many :user_hobbies
	has_many :users, through: :user_hobbies
	has_many :location_hobbies
	has_many :locations, through: :location_hobbies
	has_many :region_hobbies
	has_many :regions, through: :region_hobbies
	has_many :hobby_actions
	has_many :actions, through: :hobby_actions
	has_many :logs
end
