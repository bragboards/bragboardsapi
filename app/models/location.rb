class Location < ActiveRecord::Base
  belongs_to :user
  belongs_to :region

  has_many :location_hobbies
  has_many :hobbies, through: :location_hobbies

  has_many :picture_owners, as: :imageable
  has_many :pictures, through: :picture_owners

  has_many :logs
end
