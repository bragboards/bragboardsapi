class Log < ActiveRecord::Base
  belongs_to :user
  belongs_to :hobby
  belongs_to :location
  delegate :category, to: :hobby
  has_many :record_sets, :dependent => :destroy
  has_many :users, through: :record_sets
end