class Picture < ActiveRecord::Base
	mount_base64_uploader :avatar, ImageUploader
	has_many :picture_owners, dependent: :destroy
end
