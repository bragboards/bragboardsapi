class PictureOwner < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true
  belongs_to :picture
end
