class Qualifier < ActiveRecord::Base
  belongs_to :actions
  has_many :value_data
end
