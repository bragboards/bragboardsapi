class Record < ActiveRecord::Base
  belongs_to :record_set
  belongs_to :action
  
  has_many :value_datum, dependent: :destroy
  has_many :picture_owners, as: :imageable
  has_many :pictures, through: :picture_owners
  
  accepts_nested_attributes_for :value_datum, allow_destroy: true
end
