class RecordSet < ActiveRecord::Base
  belongs_to :user
  belongs_to :log
  has_many :records, -> { order(:id => :desc) }
  delegate :location, to: :log
  delegate :hobby, to: :log
  delegate :category, to: :log
end
