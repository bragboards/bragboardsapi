class Region < ActiveRecord::Base
	has_many :locations
	has_many :region_hobbies
	has_many :hobbies, through: :region_hobbies
end
