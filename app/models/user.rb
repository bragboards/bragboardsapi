class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  has_many :friendships
  has_many :requested_friends, -> {where('friendships.inverse_id IS NULL')},class_name: "Friendship", foreign_key: "user_id"

  has_many :friends, -> {where('friendships.inverse_id IS NOT NULL')}, :through => :friendships
	
	has_many :requests, -> {where('friendships.inverse_id IS NULL')},class_name: "Friendship", foreign_key: "friend_id"

  has_many :user_hobbies, :dependent => :destroy
  has_many :hobbies, :through => :user_hobbies

  has_many :locations
  has_many :regions, through: :locations

  has_many :picture_owners, as: :imageable
  has_many :pictures, through: :picture_owners

  has_many :logs
  has_many :record_sets

end
