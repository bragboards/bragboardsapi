class UserHobby < ActiveRecord::Base
	belongs_to :user
	belongs_to :hobby

	has_many :picture_owners, as: :imageable
  has_many :pictures, through: :picture_owners
end	
