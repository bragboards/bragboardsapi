class ValueDatum < ActiveRecord::Base
  belongs_to :record
  belongs_to :qualifier
end
