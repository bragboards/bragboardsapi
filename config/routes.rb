Rails.application.routes.draw do

	mount_devise_token_auth_for 'User', at: 'api/auth'
	
  namespace :api do
    resources :brags
    resources :categories, only: [:index, :show] do
      resources :hobbies, only: [:index, :show]
    end
    namespace :admin do
      resources :categories do
        resources :hobbies
      end
      resources :hobbies
    end
    resources :users, only: [:index, :show, :search] do
      collection do
        get 'search'
      end
      resources :friendships, only: [:create, :update, :destroy]
      resources :hobbies, only: [:index, :show]
      resources :user_hobbies
      resources :brags
      resources :locations do 
        resources :pictures
      end
      resources :pictures
      resources :logs
    end
    resources :friendships
    resources :user_hobbies do 
      resources :pictures
    end
    resources :regions do 
      resources :locations do 
        resources :pictures
      end
    end
    resources :locations do 
      resources :pictures
    end
    resources :pictures
    resources :picture_owners
    resources :logs
    resources :record_sets, only: [:index, :create, :destroy]
    resources :records
    resources :actions, only: [:show]
    resources :qualifiers, only: [:show]
    resources :value_datum
  end
end
