class AddUserInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :latitude, :float, precision: 12, scale:8
    add_column :users, :longitude, :float, precision: 12, scale:8
    add_column :users, :dob, :date
  end
end
