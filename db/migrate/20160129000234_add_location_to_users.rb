class AddLocationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :location, :string
    remove_column :users, :name
    remove_column :users, :nickname
  end
end
