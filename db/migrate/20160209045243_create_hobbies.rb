class CreateHobbies < ActiveRecord::Migration
  def change
    create_table :hobbies do |t|
      t.integer :category_id
      t.string :name

      t.timestamps null: false
    end
  end
end
