class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :name
      t.string :geolocation
      t.decimal :latitude, :float, precision: 12, scale:8
      t.decimal :longitude, :float, precision: 12, scale:8
      t.text :description
      t.integer :radius
      t.string :image

      t.timestamps null: false
    end
  end
end
