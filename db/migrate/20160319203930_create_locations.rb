class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.references :user, index: true, foreign_key: true
      t.references :region, index: true, foreign_key: true
      t.string :name
      t.string :geolocation
      t.decimal :latitude, :float, precision: 12, scale:8
      t.decimal :longitude, :float, precision: 12, scale:8
      t.text :description
      t.integer :radius
      t.string :image

      t.timestamps null: false
    end
  end
end
