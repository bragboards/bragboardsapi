class CreateRegionHobbies < ActiveRecord::Migration
  def change
    create_table :region_hobbies do |t|
      t.references :region, index: true, foreign_key: true
      t.references :hobby, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
