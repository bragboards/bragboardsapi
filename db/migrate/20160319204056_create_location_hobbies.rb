class CreateLocationHobbies < ActiveRecord::Migration
  def change
    create_table :location_hobbies do |t|
      t.references :location, index: true, foreign_key: true
      t.references :hobby, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
