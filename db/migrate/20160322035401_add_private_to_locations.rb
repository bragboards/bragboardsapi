class AddPrivateToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :private, :boolean
  end
end
