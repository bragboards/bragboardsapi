class MakeLocationDataPrecise < ActiveRecord::Migration
  def change
  	remove_column :locations, :latitude, :decimal
  	remove_column :locations, :longitude, :decimal
	 	add_column :locations, :latitude, :float, precision: 25, scale:8
	 	add_column :locations, :longitude, :float, precision: 25, scale:8
  end
end
