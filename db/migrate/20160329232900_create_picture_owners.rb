class CreatePictureOwners < ActiveRecord::Migration
  def change
    create_table :picture_owners do |t|
      t.references :imageable, polymorphic: true, index: true
      t.references :picture, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
