class CreateQualifiers < ActiveRecord::Migration
  def change
    create_table :qualifiers do |t|
      t.references :action, index: true, foreign_key: true
      t.string :name
      t.string :datatype

      t.timestamps null: false
    end
  end
end
