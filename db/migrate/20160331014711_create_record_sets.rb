class CreateRecordSets < ActiveRecord::Migration
  def change
    create_table :record_sets do |t|
      t.references :user, index: true, foreign_key: true
      t.references :log, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
