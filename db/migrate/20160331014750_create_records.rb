class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.references :record_set, index: true, foreign_key: true
      t.references :action, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
