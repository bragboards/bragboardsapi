class CreateValueData < ActiveRecord::Migration
  def change
    create_table :value_data do |t|
      t.references :record, index: true, foreign_key: true
      t.references :qualifier, index: true, foreign_key: true
      t.string :value

      t.timestamps null: false
    end
  end
end
