class CreateHobbyActions < ActiveRecord::Migration
  def change
    create_table :hobby_actions do |t|
      t.references :hobby, index: true, foreign_key: true
      t.references :action, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
