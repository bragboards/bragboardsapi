# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160701014416) do

  create_table "actions", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "brags", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.string   "body",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "friendships", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "friend_id",  limit: 4
    t.integer  "inverse_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "hobbies", force: :cascade do |t|
    t.integer  "category_id", limit: 4
    t.string   "name",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "hobby_actions", force: :cascade do |t|
    t.integer  "hobby_id",   limit: 4
    t.integer  "action_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "hobby_actions", ["action_id"], name: "index_hobby_actions_on_action_id", using: :btree
  add_index "hobby_actions", ["hobby_id"], name: "index_hobby_actions_on_hobby_id", using: :btree

  create_table "location_hobbies", force: :cascade do |t|
    t.integer  "location_id", limit: 4
    t.integer  "hobby_id",    limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "location_hobbies", ["hobby_id"], name: "index_location_hobbies_on_hobby_id", using: :btree
  add_index "location_hobbies", ["location_id"], name: "index_location_hobbies_on_location_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.integer  "region_id",   limit: 4
    t.string   "name",        limit: 255
    t.string   "geolocation", limit: 255
    t.decimal  "float",                     precision: 12, scale: 8
    t.text     "description", limit: 65535
    t.integer  "radius",      limit: 4
    t.string   "image",       limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.boolean  "private"
    t.float    "latitude",    limit: 24
    t.float    "longitude",   limit: 24
  end

  add_index "locations", ["region_id"], name: "index_locations_on_region_id", using: :btree
  add_index "locations", ["user_id"], name: "index_locations_on_user_id", using: :btree

  create_table "logs", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.integer  "hobby_id",    limit: 4
    t.integer  "location_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "logs", ["hobby_id"], name: "index_logs_on_hobby_id", using: :btree
  add_index "logs", ["location_id"], name: "index_logs_on_location_id", using: :btree
  add_index "logs", ["user_id"], name: "index_logs_on_user_id", using: :btree

  create_table "picture_owners", force: :cascade do |t|
    t.integer  "imageable_id",   limit: 4
    t.string   "imageable_type", limit: 255
    t.integer  "picture_id",     limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "picture_owners", ["imageable_type", "imageable_id"], name: "index_picture_owners_on_imageable_type_and_imageable_id", using: :btree
  add_index "picture_owners", ["picture_id"], name: "index_picture_owners_on_picture_id", using: :btree

  create_table "pictures", force: :cascade do |t|
    t.string   "avatar",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "qualifiers", force: :cascade do |t|
    t.integer  "action_id",  limit: 4
    t.string   "name",       limit: 255
    t.string   "datatype",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "qualifiers", ["action_id"], name: "index_qualifiers_on_action_id", using: :btree

  create_table "record_sets", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "log_id",     limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "record_sets", ["log_id"], name: "index_record_sets_on_log_id", using: :btree
  add_index "record_sets", ["user_id"], name: "index_record_sets_on_user_id", using: :btree

  create_table "records", force: :cascade do |t|
    t.integer  "record_set_id", limit: 4
    t.integer  "action_id",     limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "comment",       limit: 255
  end

  add_index "records", ["action_id"], name: "index_records_on_action_id", using: :btree
  add_index "records", ["record_set_id"], name: "index_records_on_record_set_id", using: :btree

  create_table "region_hobbies", force: :cascade do |t|
    t.integer  "region_id",  limit: 4
    t.integer  "hobby_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "region_hobbies", ["hobby_id"], name: "index_region_hobbies_on_hobby_id", using: :btree
  add_index "region_hobbies", ["region_id"], name: "index_region_hobbies_on_region_id", using: :btree

  create_table "regions", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "geolocation", limit: 255
    t.decimal  "latitude",                  precision: 12, scale: 8
    t.decimal  "float",                     precision: 12, scale: 8
    t.decimal  "longitude",                 precision: 12, scale: 8
    t.text     "description", limit: 65535
    t.integer  "radius",      limit: 4
    t.string   "image",       limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  create_table "user_hobbies", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "hobby_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               limit: 255,   default: "email", null: false
    t.string   "uid",                    limit: 255,   default: "",      null: false
    t.string   "encrypted_password",     limit: 255,   default: "",      null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.string   "image",                  limit: 255
    t.string   "email",                  limit: 255
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.float    "latitude",               limit: 24
    t.float    "longitude",              limit: 24
    t.date     "dob"
    t.string   "location",               limit: 255
    t.integer  "role",                   limit: 4
  end

  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree

  create_table "value_data", force: :cascade do |t|
    t.integer  "record_id",    limit: 4
    t.integer  "qualifier_id", limit: 4
    t.string   "value",        limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "value_data", ["qualifier_id"], name: "index_value_data_on_qualifier_id", using: :btree
  add_index "value_data", ["record_id"], name: "index_value_data_on_record_id", using: :btree

  add_foreign_key "hobby_actions", "actions"
  add_foreign_key "hobby_actions", "hobbies"
  add_foreign_key "location_hobbies", "hobbies"
  add_foreign_key "location_hobbies", "locations"
  add_foreign_key "locations", "regions"
  add_foreign_key "locations", "users"
  add_foreign_key "logs", "hobbies"
  add_foreign_key "logs", "locations"
  add_foreign_key "logs", "users"
  add_foreign_key "picture_owners", "pictures"
  add_foreign_key "qualifiers", "actions"
  add_foreign_key "record_sets", "logs"
  add_foreign_key "record_sets", "users"
  add_foreign_key "records", "actions"
  add_foreign_key "records", "record_sets"
  add_foreign_key "region_hobbies", "hobbies"
  add_foreign_key "region_hobbies", "regions"
  add_foreign_key "value_data", "qualifiers"
  add_foreign_key "value_data", "records"
end
