# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# User.create([
# 	{first_name: 'Ethan', 
# 		last_name: 'Stephens', 
# 		email: 'vladodex@outlook.com', 
# 		password: 'password',
# 		dob: '1989-05-03',
# 		role: 0},
# 	{first_name: 'Sean',
# 		last_name: 'Digiovanni',
# 		email: 'sean@outlook.com',
# 		password: 'password',
# 		dob: '1975-01-07',
# 		role: 0},
# 	{first_name: 'James',
# 		last_name: 'Stephens',
# 		email: 'James@outlook.com',
# 		password: 'password',
# 		dob: '1960-02-03',
# 		role: 1}
	
# 	])