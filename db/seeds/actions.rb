Action.create([
#  1
  {name: 'Kill',
    qualifiers_attributes:[
      {name: 'Breed',
        datatype: 'text'},
      {name: 'Size',
        datatype: 'number'}]},
#  2
  {name: 'Catch',
    qualifiers_attributes:[
      {name: 'Breed',
        datatype: 'text'},
      {name: 'Length',
        datatype: 'number'},
      {name: 'Pounds',
        datatype: 'number'},
      {name: 'Ounces',
        datatype: 'number'}]},
#  3
  {name: 'Match',
    qualifiers_attributes:[
      {name: 'Score',
        datatype: 'number'}]},
#  4
  {name: 'Game',
    qualifiers_attributes:[
      {name: 'Score',
        datatype: 'number'}]},
#  5
  {name: 'Throw',
    qualifiers_attributes:[
      {name: "Event",
        datatype: "text"},
      {name: "Distance",
        datatype: "number"},
      {name: "Units",
        datatype: "text"}]},
#  6
  {name: "Trick",
    qualifiers_attributes:[
      {name: "Name",
        datatype: 'text'},
      {name: "Rating",
        datatype: 'rating'}]},
#  7
  {name: "Obsticle",
    qualifiers_attributes:[
      {name: "Name",
        datatype: 'text'},
      {name: "Rating",
        datatype: 'rating'}]},
#  8
  {name: "Run",
    qualifiers_attributes:[
      {name: "Distance",
        datatype: "number"},
      {name: "Duration",
        datatype: 'time'}]},
  #9
  {name: "Ride",
    qualifiers_attributes:[
      {name: "Distance",
        datatype: "number"},
      {name: "Duration",
        datatype: 'time'}]},
  #10
  {name: "Sighting",
    qualifiers_attributes:[
      {name: "Breed",
        datatype: "text"}]}
  ])