Category.create([
#  1
	{name: 'Hunting',
		hobbies_attributes:[
#      1
			{name: 'Small Game'},
#      2
			{name: 'Large Game'},
#      3
			{name: 'Water Fowl'},
#      4
			{name: 'Exotic'}]},
#  2
	{name: 'Fishing',
		hobbies_attributes:[
#      5
			{name: 'Fresh Water'},
#      6
			{name: 'Salt Water'},
#      7
			{name: 'Exotic'}]},
#  3
	{name: 'Organized Sports',
		hobbies_attributes:[
#      8
			{name: 'Football'},
#      9
			{name: 'Baseball'},
#      10
			{name: 'Basketball'},
#      11
			{name: 'Soccer'},
#      12
			{name: 'Tennis'},
#      13
			{name: 'Volleyball'},
#      14
			{name: 'Track & Field'},
#      15
			{name: 'Bowling'}]},
#  4
	{name: 'Action Sports',
		hobbies_attributes:[
#      16
			{name: 'Water Sports'},
#      17
			{name: 'Winter Sports'},
#      18
			{name: 'Street Sports'},
#      19
			{name: 'Offroading'},
#      20
			{name: 'Extreme Sports'}]},
#  5
	{name: 'Leisure Activities',
		hobbies_attributes:[
#      21
			{name: 'Bird Watching'},
#      22
			{name: 'Kayaking'},
#      23
			{name: 'Hiking'},
#      24
			{name: 'Biking'},
#      25
			{name: 'Running'}]},
#  6
	{name: 'Restoration',
		hobbies_attributes:[
#      26
			{name: 'Automotive'},
#      27
			{name: 'Home'},
#      28
			{name: 'Antique'},
#      29
			{name: 'Collectables'}]}
	])