HobbyAction.create([
  #  1 - Small Game - Kill, Sighting
  {hobby_id: 1, action_id: 1},
  {hobby_id: 1, action_id: 10},
  #  2 - Large Game - Kill, Sighting
  {hobby_id: 2, action_id: 1},
  {hobby_id: 2, action_id: 10},
  #  3 - Water Fowl - Kill, Sighting
  {hobby_id: 3, action_id: 1},
  {hobby_id: 3, action_id: 10},
  #  4 - Exotic - Kill, Sighting
  {hobby_id: 4, action_id: 1},
  {hobby_id: 4, action_id: 10},
  #  5 - Fresh Water - Catch
  {hobby_id: 5, action_id: 2},
  #  6 - Salt Water - Catch
  {hobby_id: 6, action_id: 2},
  #  7 - Exotic - Catch
  {hobby_id: 7, action_id: 2},
  #  8 - Football - Game
  {hobby_id: 8, action_id: 4},
  #  9 - Baseball - Game
  {hobby_id: 9, action_id: 4},
  #  10 - Basketball - Game
  {hobby_id: 10, action_id: 4},
  #  11 - Soccer - Game
  {hobby_id: 11, action_id: 4},
  #  12 - Tennis - Match
  {hobby_id: 12, action_id: 3},
  #  13 - Volleyball - Match
  {hobby_id: 13, action_id: 3},
  #  14 - Track & Field - Match, Throw
  {hobby_id: 14, action_id: 3},
  {hobby_id: 14, action_id: 5},
  #  15 - Bowling - Match
  {hobby_id: 15, action_id: 3},
  #  16 - Water Sports - Trick
  {hobby_id: 16, action_id: 6},
  #  17 - Winter Sports - Trick
  {hobby_id: 17, action_id: 6},
  #  18 - Street Sports - Trick
  {hobby_id: 18, action_id: 6},
  #  19 - Offroading - Ride, Obsticle
  {hobby_id: 19, action_id: 9},
  {hobby_id: 19, action_id: 7},
  #  21 - Bird Watching - Sighting
  {hobby_id: 21, action_id: 10},
  #  22 - Kayaking - Ride
  {hobby_id: 22, action_id: 9},
  #  23 - Hiking - Run
  {hobby_id: 23, action_id: 8},
  #  24 - Biking - Ride
  {hobby_id: 24, action_id: 9},
  #  25 - Running - Run
  {hobby_id: 25, action_id: 8}
  ])