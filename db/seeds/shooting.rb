Action.create([
#  11
  {name: 'Target',
    qualifiers_attributes:[
      {name: 'Distance',
        datatype: 'text'},
      {name: 'Size',
        datatype: 'number'},
      {name: 'Type',
        datatype: 'text'},
      {name: 'Score',
        datatype: 'number'}]
    },
#  12
  {name: 'Round',
    qualifiers_attributes:[
      {name: 'Overall Score',
        datatype: 'number'},
      {name: "Twelves/X's",
        datatype: 'number'}]
    },
#  13
  {name: 'Round',
    qualifiers_attributes:[
      {name: 'Possible Total',
        datatype: 'number'},
      {name: 'Score',
        datatype: 'number'}]
    }
  ])

Category.create([
#  7
  {name: 'Marksmanship',
		hobbies_attributes:[
#      30
      {name: 'Archery'},
#      31
      {name: 'Target Shooting'},
#      32
      {name: 'Skeet'}]
    }
  ])

HobbyAction.create([
  #  30 - Archery - Target, Round
  {hobby_id: 30, action_id: 11},
  {hobby_id: 30, action_id: 12},
  #  30 - Target Shooting - Target, Round
  {hobby_id: 31, action_id: 11},
  {hobby_id: 31, action_id: 13},
  #  30 - Skeet Shooting - Target, Round
  {hobby_id: 32, action_id: 11},
  {hobby_id: 32, action_id: 13}
  ])
  