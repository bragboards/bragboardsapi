FactoryGirl.define do
	factory :users do |user|
	  user.first_name            "Mark"
	  user.last_name			 "Brown"
	  user.address1				 "101 Fairy Lane"
	  user.city					 "Pleasantville"
	  user.state				 "Texas"
	  user.zipcode				 70506
	  user.email                 "mbrown@yahoo.com"
	  user.password               "foobar"
	  user.password_confirmation  "foobar"
	end
end