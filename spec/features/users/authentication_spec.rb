# spec/features/users/authentication_spec.rb

require 'spec_helper'

describe 'Authentication', js: true do
  describe 'login' do
    context 'with valid inputs' do
      @user = FactoryGirl.create(:users)
      visit '/sign_in'
      fill_in "Email", with: @user.email
      fill_in "Password", with: @user.password
      find("button", text: "Sign in").click

      expect(page).to have_content('Sign out')
    end
  end
end