require 'test_helper'

class Api::BragsControllerTest < ActionController::TestCase
  setup do
    @brag = brags(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:brags)
  end

  test "should create brag" do
    assert_difference('Brag.count') do
      post :create, brag: {  }
    end

    assert_response 201
  end

  test "should show brag" do
    get :show, id: @brag
    assert_response :success
  end

  test "should update brag" do
    put :update, id: @brag, brag: {  }
    assert_response 204
  end

  test "should destroy brag" do
    assert_difference('Brag.count', -1) do
      delete :destroy, id: @brag
    end

    assert_response 204
  end
end
